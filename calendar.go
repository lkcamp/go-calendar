package main

import (
	"github.com/go-redis/redis"
	"net/http"
	"log"
	"encoding/json"
	"regexp"
	"errors"
	"html/template"
)

type Date_ struct {
	Year string
	Month string
	Day string
}

type Event struct {
	Title string
	Date string
	Description string
}

func (d *Date_) Format() string {
	return d.Year + "/" + d.Month + "/" + d.Day
}

func (e *Event) parseDate()(Date_, error) {
	re := regexp.MustCompile("([0-9]{4})/([0-9]{2})/([0-9]{2})")
	match := re.FindAllStringSubmatch(e.Date, -1)
	if match == nil {
		return Date_{}, errors.New("Invalid date format")//some error
	}
	if len(match[0]) != 4 {
		return Date_{}, errors.New("Invalid date format")//some error
	}
	dateParsed := Date_{}
	dateParsed.Year = match[0][1]
	dateParsed.Month = match[0][2]
	dateParsed.Day = match[0][3]
	return dateParsed, nil
}

func (e *Event) Save() error {
	dateParsed, err := e.parseDate()
	if err != nil {
		return err
	}
	key := dateParsed.Format()
	redisdb := redis.NewClient(&redis.Options{
	    Addr: ":6379",
	    })
	sevent, err := json.Marshal(e)
	if err != nil {
		return err
	}
	_, err = redisdb.SAdd(key, sevent).Result()
	if err != nil {
		redisdb.Close()
		return errors.New("Ble")
	}
	redisdb.Close()
	return nil
}

func loadDateEvents(date string)([]Event, error) {
	redisdb := redis.NewClient(&redis.Options{
	    Addr: ":6379",
	    })
	events := []Event{}
	event := Event{}
	iter := redisdb.SScan(date, 0, "", 0 ).Iterator()
	for iter.Next() {
		err := json.Unmarshal([]byte(iter.Val()), &event)
		if err != nil {
			return []Event{}, err
		}
		events = append(events, event)
	}
	return events, nil
}

func loadMonthEvents(month string)([]Event, error) {
	redisdb := redis.NewClient(&redis.Options{
		Addr: ":6379",
		})
	events := []Event{}
	event := Event{}
	dates,err := redisdb.Keys(month+"/*").Result()
	 if err != nil {
		return []Event{}, err
	}
	for _,date := range dates {
		iter := redisdb.SScan(date, 0, "", 0).Iterator()
		for iter.Next() {
			err := json.Unmarshal([]byte(iter.Val()), &event)
			if err != nil {
				return []Event{}, err
			}
			events = append(events, event)
		}
	}
	return events, nil
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Path[len("/view/"):]
	es := []Event{}
	if query[:len("month/")] == "month/" {
		es, _ = loadMonthEvents(query[len("month/"):])
	} else {
		es, _ = loadDateEvents(query)
	}
	p := struct {
		Events []Event
	}{
		Events: es,
	}
	t, _ := template.ParseFiles("templates/calc.html")
	t.Execute(w, p)
}

func loadEvent(date string)(Event, error) {
	return Event{Date: date}, nil
}

func addHandler(w http.ResponseWriter, r *http.Request) {
	date := r.URL.Path[len("/add/"):]
	p, err := loadEvent(date)
	if err != nil {
		p = Event{}
	}
	t, _ := template.ParseFiles("templates/add.html")
	t.Execute(w, p)
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	date := r.FormValue("date")
	title := r.FormValue("title")
	desc := r.FormValue("description")
	e := &Event{Title: title, Date: date, Description: desc}
	err := e.Save()
	if err != nil {
		http.Redirect(w, r, "", http.StatusNotFound)
	}
	http.Redirect(w, r, "/view/"+ date, http.StatusFound)
}

func main() {
	http.HandleFunc("/view/", viewHandler)
	http.HandleFunc("/add/", addHandler)
	http.HandleFunc("/save/", saveHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
